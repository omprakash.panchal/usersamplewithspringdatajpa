package com.UserCrudSpringDataJpa.controller;

import com.UserCrudSpringDataJpa.entity.User;
import com.UserCrudSpringDataJpa.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.Optional;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @MockBean
    private UserService userService;

    @Autowired
    private MockMvc mockMvc;

    User user;

    @BeforeEach
    public void init() {
        user = User.builder().firstName("Omprakash").middleName("bhimrao").lastName("panchal").email("om@gmail.com").contactNumber("1234567898").address("solapur").build();

    }

    // Test For AddUser

    @Test
    public void addUSerTest() throws Exception {
        Mockito.when(userService.createUser(Mockito.any())).thenReturn(user);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/users").contentType(MediaType.APPLICATION_JSON).content("{\n" + "    \"firstName\":\"Omprakash\",\n" + "    \"middleName\":\"Bhimrao\",\n" + "    \"lastName\":\"Panchal\",\n" + "    \"email\":\"om@gmail.com\",\n" + "    \"contactNumber\":\"12343212543\",\n" + "    \"address\":\"solapur\"\n" + "}")).andExpect(MockMvcResultMatchers.status().isCreated());

    }

    @Test
    public void updateUserTest() throws Exception {
        Long id = 89L;
        Mockito.when(userService.getUseById(89L)).thenReturn(Optional.of(user));

        this.mockMvc.perform(MockMvcRequestBuilders.put("/users/update/89")
                .contentType(MediaType.APPLICATION_JSON).content("{\n" + "    \"firstName\":\"Shantabai\",\n" + "  " +
                        "  \"middleName\":\"Bhimrao\",\n" + "  " +
                        "  \"lastName\":\"Panchal\",\n" + "  " +
                        "  \"email\":\"om@gmail.com\",\n" + "  " +
                        "  \"contactNumber\":\"12343212543\",\n" + "   " +
                        " \"address\":\"solapur\"\n" + "}"))
                .andExpect(MockMvcResultMatchers.status().isOk());
                 System.out.println(user.getFirstName());

    }


    // Test For getUser


    @Test
    public void getUsersTest() throws Exception {
        Mockito.when(userService.getUser()).thenReturn(List.of(user));

        this.mockMvc.perform(MockMvcRequestBuilders.get("/users")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
                System.out.println(user);

    }

    // GEt USer By ID
    @Test
    public void getUserByIDTest() throws Exception {
        Long id = 89L;
        Mockito.when(userService.getUseById(89L)).thenReturn(Optional.of(user));

        this.mockMvc.perform(MockMvcRequestBuilders.get("/users/89")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isFound());
                 System.out.println(user);
    }


    // GEt USer By ID
    @Test
    public void getUserByEmailTest() throws Exception {
        String email = "om@gmail.com";
        Mockito.when(userService.getUserByEmail("om@gmail.com")).thenReturn(Optional.of(user));

        this.mockMvc.perform(MockMvcRequestBuilders.get("/users/email/om@gmail.com")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteUserTest() throws Exception {
        Long id = 89L;
        Mockito.when(userService.getUseById(89L)).thenReturn(Optional.of(user));

        this.mockMvc.perform(MockMvcRequestBuilders.delete("/users/delete/89").contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());
        System.out.println(user);
    }


}
