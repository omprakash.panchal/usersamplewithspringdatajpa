package com.UserCrudSpringDataJpa.services;

import com.UserCrudSpringDataJpa.entity.User;
import com.UserCrudSpringDataJpa.repositories.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@SpringBootTest
public class UserServiceTests {

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    User user;

    @BeforeEach
    public void init() {
        user = User.builder()
                .firstName("Omprakash")
                .middleName("bhimrao")
                .lastName("panchal")
                .email("om@gmail.com")
                .contactNumber("1234567898")
                .address("solapur")
                .build();

    }

    //Test For CreateUser
    @Test
    public void createUserTest() {
        Mockito.when(userRepository.save(Mockito.any())).thenReturn(user);
        User user1 = userService.createUser(user);
        Assertions.assertNotNull(user1, "User1 is NULL ...!! Test Case Failed...!!");
        Assertions.assertEquals("Omprakash", user1.getFirstName(), "User Fisrt Name is Not Equal ..!! Test Case Failed...!!");
        System.out.println(user1.getFirstName());

    }


    //Test For updateUser
    @Test
    public void updateUSerTest() {
        Long id = 123L;

        User user1 = User.builder()
                .firstName("Bhimrao")
                .middleName("keshav")
                .lastName("Panchal")
                .email("bhimrao@gmail.com")
                .contactNumber("9898989898")
                .address("solapuer")

                .build();

        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(user));
        Mockito.when(userRepository.save(Mockito.any())).thenReturn(user);

        Assertions.assertNotNull(userService.updateUSer(id, user1));

        System.out.println(user.getFirstName() + " \n" +
                user.getMiddleName());

    }

    //Test For GetUSerByID
    @Test
    public void getUseByIdTest() {
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(user));
        Mockito.when(userService.getUseById(Mockito.any())).thenReturn(Optional.of(user));
        Assertions.assertNotNull(user, "User1 is NULL ...!! Test Case Failed...!!");

    }

    @Test
    public void getUserByEmailTest() {
        Mockito.when(userRepository.findByEmail(Mockito.any())).thenReturn(Optional.of(user));
        Mockito.when(userService.getUserByEmail(Mockito.any())).thenReturn(Optional.of(user));
        Assertions.assertNotNull(user, "User1 is NULL ...!! Test Case Failed...!!");

    }

    @Test
    public void deleteUserTest() throws Exception {
        Long id = 89L;

        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(user));
        userService.deleteUser(id);
        Mockito.verify(userRepository, Mockito.times(1)).delete(user);


    }

}
