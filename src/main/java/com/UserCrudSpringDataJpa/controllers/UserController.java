package com.UserCrudSpringDataJpa.controllers;

import com.UserCrudSpringDataJpa.entity.User;
import com.UserCrudSpringDataJpa.helper.MessageResponse;
import com.UserCrudSpringDataJpa.services.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

    @Autowired
    private UserService userService;


    @PostMapping("/users")
    public ResponseEntity<User> addUSer(@Valid @RequestBody User user) {
        User addedUser = userService.createUser(user);
        return new ResponseEntity<>(addedUser, HttpStatus.CREATED);
    }


    //Get All Users
    @GetMapping("/users")
    public ResponseEntity<List<User>> getUsers() {
        return new ResponseEntity<>(userService.getUser(), HttpStatus.OK);
    }

    //Get USerBy Id
    @GetMapping("/users/{id}")
    public ResponseEntity<Optional<User>> getUserByID(@PathVariable Long id) {

        Optional<User> user1 = userService.getUseById(id);
        return new ResponseEntity<>(user1, HttpStatus.FOUND);
    }

    //Delete User By Id
    @DeleteMapping("/users/delete/{id}")
    public ResponseEntity<MessageResponse> deleteUser(@PathVariable Long id) throws Exception {
        userService.deleteUser(id);
        MessageResponse message = MessageResponse.builder().message("User Deleted Succfully").success(true).httpStatus(HttpStatus.OK).build();
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    //Update User
    @PutMapping("/users/update/{id}")
    public ResponseEntity<User> updateUser(@PathVariable Long id, @Valid @RequestBody User user) {
        return new ResponseEntity<>(userService.updateUSer(id, user),HttpStatus.OK);
    }

    //GEt User By Email
    @GetMapping("/users/email/{email}")
    public Optional<User> getByEmail(@PathVariable String email) {
        return userService.getUserByEmail(email);
    }

    //Search User By First Name And Last Name

    @GetMapping("/users/search/{keywords}")
    public List<User> searchUserByFUllName(@PathVariable String keywords) {
        return userService.searchUser(keywords);
    }

}
