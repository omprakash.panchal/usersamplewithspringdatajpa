package com.UserCrudSpringDataJpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserCrudSpringDataJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserCrudSpringDataJpaApplication.class, args);
	}

}
