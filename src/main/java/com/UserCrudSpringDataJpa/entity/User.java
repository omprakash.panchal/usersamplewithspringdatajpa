package com.UserCrudSpringDataJpa.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import jdk.jfr.Enabled;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(min = 2, max = 50, message = "Invalid Fisrt Name..!!")
    private String firstName;

    @Size(min = 3, max = 50, message = "Invalid Middle Name..!!")
    private String middleName;

    @Size(min = 3, max = 50, message = "Invalid Last Name..!!")
    private String lastName;


    @Email(message = "Invalid Email")
    private String email;

    @Size(min = 8, max = 12, message = "Invalid Contact Number..!!")
    private String contactNumber;

    @Size(min = 3, max = 50, message = "Invalid Address..!!")
    @NotBlank(message = "Please Enter Address..!!")
    private String address;
}
