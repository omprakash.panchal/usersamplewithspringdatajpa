package com.UserCrudSpringDataJpa.services.impl;

import com.UserCrudSpringDataJpa.entity.User;
import com.UserCrudSpringDataJpa.exception.ResourceNotFoundException;
import com.UserCrudSpringDataJpa.repositories.UserRepository;
import com.UserCrudSpringDataJpa.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;


    //Create User
    @Override
    public User createUser(User user) {
        User savedUser = (User) userRepository.save(user);
        return savedUser;

    }

    //Delete User
    @Override
    public void deleteUser(Long id) throws Exception {
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User Not Found Form This ID"));
        userRepository.delete(user);

    }

    //Get All User
    @Override
    public List<User> getUser() {

        return userRepository.findAll();
    }

    //Get User By ID
    @Override
    public Optional<User> getUseById(Long id) {

        Optional<User> singleUser = Optional.ofNullable(userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User Not Found From This Id")));
        return singleUser;
    }

    //Update User
    @Override
    public User updateUSer(Long id, User user) {
        User user1 = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User Not Found From This Id"));
        user1.setFirstName(user.getFirstName());
        user1.setMiddleName(user.getMiddleName());
        user1.setLastName(user.getLastName());
        user1.setEmail(user.getEmail());
        user1.setContactNumber(user.getContactNumber());
        user1.setAddress(user.getAddress());
        User updatedUser = userRepository.save(user1);
        return updatedUser;
    }

    //Get User By Email
    @Override
    public Optional<User> getUserByEmail(String email) {
        Optional<User> userByEmail = Optional.ofNullable(userRepository.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException("User Not Founf For THis Email")));
        return userByEmail;
    }

    //Search User By FirstName
    @Override
    public List<User> searchUser(String keyword) {
        return userRepository.findByFirstNameContaining(keyword);
    }
}
