package com.UserCrudSpringDataJpa.services;

import com.UserCrudSpringDataJpa.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    //Create
    public User createUser(User user);

    //Delete
    public void deleteUser(Long id) throws Exception;

    //getAll
    public List<User> getUser();
    //getByID

    //get user by id
    public Optional<User> getUseById(Long id);

    //Update
    public User updateUSer(Long id, User user);

    //Get User By Emial

    public Optional<User> getUserByEmail(String email);

    //Search User Using Name

    public List<User> searchUser(String keyword);
}
