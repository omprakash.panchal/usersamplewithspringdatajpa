package com.UserCrudSpringDataJpa.helper;

import lombok.*;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MessageResponse {

    private String message;
    private boolean success;
    private HttpStatus httpStatus;

}
